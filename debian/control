Source: laszip
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: chrpath,
               cmake (>= 2.8.11),
               debhelper-compat (= 12)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/laszip
Vcs-Git: https://salsa.debian.org/debian-gis-team/laszip.git
Homepage: https://laszip.org/

Package: liblaszip8
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Lossless LiDAR compression - shared library
 LASzip quickly turns bulky LAS files into compact LAZ files without
 information loss.
 .
 This package contains the shared library.

Package: liblaszip-api8
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Lossless LiDAR compression - API shared library
 LASzip quickly turns bulky LAS files into compact LAZ files without
 information loss.
 .
 This package contains the API shared library.

Package: liblaszip-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblaszip8 (= ${binary:Version}),
         liblaszip-api8 (= ${binary:Version}),
         ${misc:Depends}
Description: Lossless LiDAR compression - development files
 LASzip quickly turns bulky LAS files into compact LAZ files without
 information loss.
 .
 This package contains development files.
