laszip (3.4.3-3) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.6.0, no changes.
  * Update upstream metadata.
  * Update watch file for GitHub URL changes.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
    - Drop -V from dh_makeshlibs
  * Drop undefined substitution variables.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 28 Nov 2020 13:44:40 +0100

laszip (3.4.3-2) unstable; urgency=medium

  * Drop Name field from upstream metadata.
  * Bump Standards-Version to 4.5.0, no changes.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
  * Bump watch file version to 4.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 11 Nov 2020 18:27:07 +0100

laszip (3.4.3-1) unstable; urgency=medium

  * New upstream release.
  * Update Homepage URL to use HTTPS.
  * Bump Standards-Version to 4.4.1, no changes.
  * Add lintian override for shared-lib-without-dependency-information.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 12 Nov 2019 05:41:08 +0100

laszip (3.4.1-2) unstable; urgency=medium

  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 07 Jul 2019 08:25:03 +0200

laszip (3.4.1-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 19 May 2019 17:14:17 +0200

laszip (3.4.1-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Drop buildflags.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 13 Apr 2019 06:56:58 +0200

laszip (3.4.0-1~exp1) experimental; urgency=medium

  * Initial release. (Closes: #750731)

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 01 Apr 2019 21:35:11 +0200
